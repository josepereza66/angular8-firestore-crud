# angularFirebase

Ejemplo de "CRUD" realizado con Angular y Firebase.
Solamente funciona la Version 2.0

> **UPDATE 10/03/2020:**: Se ha actualizado el repositorio a Angular 8

![Imagen Principal]<p align="center"><img src="v2.0/captura-1.jpg"> </p>

## Instalación

1. Instalar dependencias
- ```npm install```
- ```npm install --save firebase @angular/fire -f```
- ```npm install bootstrap```
- ```npm install --save font-awesome angular-font-awesome```

2. Configurar atributos del objeto **firestore** en el archivo ```environment.ts```

3. Ejecutar la aplicación

```ng serve``` // ```ng serve -o```
